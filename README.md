*****Preparation

1) mkdir data-1 data-2 data-3 key
2) chmod 700 key
3) cd key
4) openssl rand -base64 741 > keyfile.txt
5) chmod 700 keyfile.txt
6) cd ..
8) docker-compose up -d

*****Inside to mongodb-1:

1) mongo -uroot -p123456
2)
rsconf = {
    _id: "rs0",
    members: [
      { _id: 0, host: "mongodb-1.net:27017"},
      { _id: 1, host: "mongodb-2.net:27017" },
      { _id: 2, host: "mongodb-3.net:27017" }
    ]
  }
3) rs.initiate(rsconf)
4) rs.status()

*****On the slave:
1) secondaryDB.setSlaveOk() или rs.slaveOk()
